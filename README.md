# Tww

Ruby interface with TWW Unimessage API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tww'
```

And then execute:

    $ bundle

## Configuration
You can configure global username and password with configure block:

```ruby
Tww.configure do |config|
  config.username = 'FakeUsername'
  config.password = 'FakePassword'
end
```

If you are using Rails, create an initializer:

```
config/initializer/tww.rb
```

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it ( https://github.com/[my-github-username]/tww_unimessage/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
